/*
 * Copyright 2016 Arthur Zagretdinov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.azagretdinov.riderautoparts.camel.routing;

import org.apache.camel.EndpointInject;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.bitbucket.azagretdinov.riderautoparts.camel.routing.config.CamelRouts;
import org.bitbucket.azagretdinov.riderautoparts.camel.routing.model.OrderPosition;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class OrderRequestTransformationTest extends CamelJUnitTestSupport {

    private static final String CSV = "name,amount\n" + "motor,1";

    private static final String CSV_TWO_ITEMS = "name,amount\n" +
            "brake pad,2\n" +
            "gearbox,2";

    private static final String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><orders>\n" +
            "    <order name=\"motor\" amount=\"1\" />\n" +
            "</orders>";

    @EndpointInject(uri = "{{camel.jms.orders}}")
    protected MockEndpoint orderEndpoint;

    @Produce(uri = "{{camel.jms.incoming.orders}}")
    protected ProducerTemplate template;

    @Test
    public void should_transform_xml_to_order_request() throws InterruptedException {

        List<OrderPosition> orderList = new ArrayList<>(Arrays.asList(new OrderPosition("motor", 1)));

        executeTransformTest(orderList, XML);
    }

    @Test
    public void should_transform_csv_with_one_item_to_order_request() throws InterruptedException {

        List<OrderPosition> orderList = Collections.singletonList(new OrderPosition("motor", 1));

        executeTransformTest(orderList, CSV);
    }

    @Test
    public void should_transform_csv_with_two_items_to_order_request() throws InterruptedException {

        List<OrderPosition> orderList = new ArrayList<>(Arrays.asList(new OrderPosition("brake pad", 2), new OrderPosition("gearbox", 2)));

        executeTransformTest(orderList, CSV_TWO_ITEMS);
    }

    private void executeTransformTest(List<OrderPosition> orderList, String body) throws InterruptedException {
        orderEndpoint.expectedMessageCount(1);
        orderEndpoint.expectedBodiesReceived(new Object[]{orderList});

        template.sendBody(body);

        orderEndpoint.assertIsSatisfied(100);
    }


    @Override
    protected RouteBuilder createRouteBuilder() throws Exception {
        return new CamelRouts().normalizeMessageData();
    }

    @Override
    protected String getPropertyPrefix() {
        return this.getClass().getName();
    }
}
