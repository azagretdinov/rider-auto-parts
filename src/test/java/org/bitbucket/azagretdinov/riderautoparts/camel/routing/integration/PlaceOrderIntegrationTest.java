/*
 * Copyright 2016 Arthur Zagretdinov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.azagretdinov.riderautoparts.camel.routing.integration;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import org.apache.camel.*;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.spring.CamelSpringDelegatingTestContextLoader;
import org.apache.camel.test.spring.MockEndpoints;
import org.apache.camel.testng.AbstractCamelTestNGSpringContextTests;
import org.apache.camel.testng.TestSupport;
import org.bitbucket.azagretdinov.riderautoparts.camel.routing.model.OrderPosition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.google.common.io.Files.copy;
import static com.google.common.io.Files.createParentDirs;
import static org.testng.Assert.assertEquals;

@ContextConfiguration(classes = IntegrationTestSpringConfig.class, loader = CamelSpringDelegatingTestContextLoader.class, inheritLocations = false)
@MockEndpoints
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PlaceOrderIntegrationTest extends AbstractCamelTestNGSpringContextTests {

    public static final String CSV_MESSAGE_FILE = "message2.csv";
    public static final String XML_MESSAGE_FILE = "message1.xml";

    @EndpointInject(uri = "{{camel.jms.orders}}")
    protected MockEndpoint orderEndpoint;

    @Produce(uri = "jetty:{{camel.jetty.orders.http.url}}")
    protected ProducerTemplate template;

    @Autowired
    private CamelContext context;

    @PropertyInject("{{camel.file.incoming.folder}}")
    private String orderFolder;

    @BeforeMethod
    public void setUp() throws Exception {
        orderEndpoint.reset();
    }

    @Test
    public void should_process_and_put_to_order_queue_order_from_csv_file_from_directory() throws Exception {
        orderEndpointCSVExpectation();

        copyFile(CSV_MESSAGE_FILE);

        orderEndpoint.assertIsSatisfied();
    }

    @Test
    public void should_process_and_put_to_order_queue_order_from_xml_file_from_directory() throws Exception {
        orderEndpointXMLExpectation();

        copyFile(XML_MESSAGE_FILE);

        orderEndpoint.assertIsSatisfied();
    }

    @Test
    public void should_process_and_put_to_order_queue_order_from_http_xml() throws Exception {
        orderEndpointXMLExpectation();
        assertRequestProcessed(sendTestRequest(XML_MESSAGE_FILE));
    }

    @Test
    public void should_process_and_put_to_order_queue_order_from_http_csv() throws Exception {

        orderEndpointCSVExpectation();
        assertRequestProcessed(sendTestRequest(CSV_MESSAGE_FILE));
    }

    private void assertRequestProcessed(String responseString) throws InterruptedException {
        assertEquals("Ok", responseString);
        orderEndpoint.assertIsSatisfied();
    }

    private String sendTestRequest(String fileName) throws IOException {
        URL url = Resources.getResource(fileName);
        String body = Resources.toString(url, Charsets.UTF_8);

        orderEndpoint.whenAnyExchangeReceived((exchange)->{
            exchange.getIn().setBody("Ok");
        });

        Object response = template.requestBody(body);
        return context.getTypeConverter().convertTo(String.class, response);
    }


    private void orderEndpointXMLExpectation() {
        List<OrderPosition> orderList = new ArrayList<>();
        orderList.addAll(Arrays.asList(new OrderPosition("motor", 1), new OrderPosition("wheel", 10)));

        orderEndpoint.expectedMessageCount(1);
        orderEndpoint.expectedBodiesReceived(new Object[]{orderList});
    }

    private void orderEndpointCSVExpectation() {
        List<OrderPosition> orderList = new ArrayList<>();
        orderList.addAll(Arrays.asList(new OrderPosition("brake pad", 2), new OrderPosition("gearbox", 2)));

        orderEndpoint.expectedMessageCount(1);
        orderEndpoint.expectedBodiesReceived(new Object[]{orderList});
    }


    private void copyFile(String messageFile) throws URISyntaxException, IOException {
        TestSupport.deleteDirectory(orderFolder);

        final URI csvFileUri = getFrom(messageFile);
        final File to = getTo();

        createParentDirs(to);

        copy(new File(csvFileUri), to);
    }

    private File getTo() {
        return new File(orderFolder + File.separator + CSV_MESSAGE_FILE);
    }

    private URI getFrom(String messageFile) throws URISyntaxException {
        return Resources.getResource(messageFile).toURI();
    }
}
