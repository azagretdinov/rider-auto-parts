/*
 * Copyright 2016 Arthur Zagretdinov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.azagretdinov.riderautoparts.camel.routing;

import org.apache.camel.*;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.bitbucket.azagretdinov.riderautoparts.camel.routing.config.CamelRouts;
import org.junit.Test;

import java.io.IOException;

public class OrderRequestFileTest extends CamelJUnitTestSupport {

    @EndpointInject(uri = "mock://jms:incomingOrders")
    protected MockEndpoint orderEndpoint;

    @Produce
    protected ProducerTemplate template;

    @PropertyInject("{{camel.file.incoming.folder}}")
    private String incomeFolder;

    @Test
    public void should_put_file_data_to_request_order_queue() throws InterruptedException, IOException {

        orderEndpoint.expectedMessageCount(1);

        template.sendBodyAndHeader("file:" + incomeFolder, "a,b", Exchange.FILE_NAME, "message.cvs");

        orderEndpoint.assertIsSatisfied(100);
    }

    @Override
    protected RouteBuilder createRouteBuilder() throws Exception {
        return new CamelRouts().fileToJMS();
    }

}
