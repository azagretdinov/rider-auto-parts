/*
 * Copyright 2016 Arthur Zagretdinov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.azagretdinov.riderautoparts.actor

/*
 * Copyright 2016 Arthur Zagretdinov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import akka.actor.ActorSystem
import akka.camel.CamelExtension
import akka.testkit.{ImplicitSender, TestActorRef, TestKit, TestProbe}
import org.apache.camel.ExchangePattern
import org.apache.camel.builder.RouteBuilder
import org.bitbucket.azagretdinov.riderautoparts.scalatest
import org.bitbucket.azagretdinov.riderautoparts.utils.IdGenerator
import org.junit.runner.RunWith
import org.mockito.Mockito.when
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}

import scala.concurrent.duration._

@RunWith(classOf[JUnitRunner])
class RequestOrderConsumerActorTest(_system: ActorSystem)

  extends TestKit(_system)
    with ImplicitSender
    with Matchers
    with FlatSpecLike
    with BeforeAndAfterAll
    with scalatest.MockitoSugar {

  def this() = this(ActorSystem("RequestOrderConsumerActorTest"))

  val idGenerator = mock[IdGenerator];
  when(idGenerator.generate()) thenReturn "someId";

  override def afterAll: Unit = {
    import system.dispatcher
    system.terminate().foreach { _ =>
      println("Actor system was shut down")
    }
  }

  val orderQueue = "jms:orders"

  val camel = CamelExtension(system)
  val camelContext = camel.context;
  camelContext.addComponent("jms", camelContext.getComponent("seda"))
  camelContext.addRoutes(new RouteBuilder() {
    override def configure(): Unit = {
      from("direct:mockInput").to(orderQueue).tracing().id("MockRoute");
    }
  })

  val eventActor = TestProbe();

  val transformer = TestActorRef(RequestOrderActor.transformer())
  val processor = TestActorRef(RequestOrderActor.processor(transformer, eventActor.ref))
  val consumerActor = TestActorRef(RequestOrderActor.consumer(processor, idGenerator, orderQueue))

  val activationFuture = camel.activationFutureFor(consumerActor)(timeout = 5 seconds,
    executor = system.dispatcher);

  it should "receive jsm message" in {

    val producerTemplate = camelContext.createProducerTemplate();
    val response = producerTemplate.sendBody("direct:mockInput", ExchangePattern.InOut, List.empty);

    response shouldBe "someId"

    verify(idGenerator).generate()
  }

  it should "send event message" in {

    val producerTemplate = camelContext.createProducerTemplate();
    val response = producerTemplate.sendBody("direct:mockInput", ExchangePattern.InOut, List.empty);

    response shouldBe "someId"

    eventActor.expectMsgClass(classOf[RequestOrderEvent])
  }
}