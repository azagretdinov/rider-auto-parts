/*
 * Copyright 2016 Arthur Zagretdinov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.azagretdinov.riderautoparts.actor

import akka.actor.Status.Failure
import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.camel.{CamelMessage, Consumer}
import akka.pattern.ask
import akka.util.Timeout
import org.bitbucket.azagretdinov.riderautoparts.camel.routing.model.OrderPosition
import org.bitbucket.azagretdinov.riderautoparts.utils.IdGenerator
import org.springframework.beans.factory.annotation.Value

import scala.concurrent.duration._


object RequestOrderActor {

  def consumer(processor: ActorRef, idGenerator: IdGenerator, defaultEndpoint: String = ""): Props = {
    Props(new RequestOrderConsumerActor(processor, idGenerator, defaultEndpoint))
  }

  def processor(transformer: ActorRef, eventActor: ActorRef): Props = {
    Props(new RequestOrderProcessorActor(transformer, eventActor))
  }

  def transformer(): Props = {
    Props(new RequestOrderTransformer())
  }

}

class RequestOrderConsumerActor(processor: ActorRef, idGenerator: IdGenerator, defaultEndpoint: String) extends Consumer with ActorLogging {

  @Value("camel.jms.orders")
  val endpoint: String = defaultEndpoint

  override def endpointUri: String = endpoint

  override def receive: Receive = {

    case camelMessage: CamelMessage => {

      val positions = camelMessage.bodyAs[List[OrderPosition]]

      val msg = new RequestOrder(createId(), camelMessage, positions)

      processor.forward(msg)
    }
    case _ => {
      log.info("Message received")
    }
  }

  def createId(): String = {
    idGenerator.generate()
  }

}

class RequestOrderProcessorActor(transformer: ActorRef, eventActor: ActorRef) extends Actor with ActorLogging {

  implicit val timeout = Timeout(5 seconds)

  def receive = {
    case msg: RequestOrder => {
      sendEvent(msg)
      transformer.forward(msg);
    }
    case msg: Failure => sender() ! msg
  }

  def sendEvent(requestOrder: RequestOrder): Unit = {
    try {
      val requestOrderEvent = new RequestOrderEvent(requestOrder.id,requestOrder.bodyAsList)
      eventActor ? requestOrderEvent
    } catch {
      case e: Exception => {
        log.error(e, e.getMessage)
        sender() ! akka.actor.Status.Failure(e);
      }
    }
  }
}

class RequestOrderTransformer() extends Actor with ActorLogging {
  def receive = {
    case msg: RequestOrder =>
      val camelMessage = msg.camelMessage;

      log.debug("Order ID: {}", msg.id)

      sender() ! camelMessage.withBody(msg.id)
    case msg: Failure => sender() ! msg
  }

}

case class RequestOrder(id: String, camelMessage: CamelMessage, bodyAsList: List[OrderPosition])