/*
 * Copyright 2016 Arthur Zagretdinov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.azagretdinov.riderautoparts.camel.routing.config;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.bitbucket.azagretdinov.riderautoparts.camel.routing.model.OrderPosition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 *
 */
@Configuration
public class CamelRouts {

    @Bean
    public RouteBuilder fileToJMS() {
        return new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from("file:{{camel.file.incoming.folder}}").to("{{camel.jms.incoming.orders}}").routeId("FileToJMS");
            }
        };
    }

    @Bean
    public RouteBuilder normalizeMessageData() {

        return new RouteBuilder() {

            @Override
            public void configure() throws Exception {
                JaxbDataFormat jaxb = new JaxbDataFormat("org.bitbucket.azagretdinov.riderautoparts.camel.routing.model");

                BindyCsvDataFormat csv = new BindyCsvDataFormat(OrderPosition.class);

                String jmsQueue = "{{camel.jms.orders}}";
                from("{{camel.jms.incoming.orders}}").tracing()
                                                     .convertBodyTo(String.class)
                                                     .choice()
                                                     .when(simple("${body} contains '?xml'"))
                                                     .unmarshal(jaxb)
                                                     .transform(simple("${body.orders}"))
                                                     .inOut(jmsQueue)
                                                     .otherwise()
                                                     .unmarshal(csv)
                                                     .choice()
                                                     .when(bodyAs(OrderPosition.class))
                                                     .convertBodyTo(Iterable.class)
                                                     .convertBodyTo(List.class)
                                                     .inOut(jmsQueue)
                                                     .otherwise()
                                                     .inOut(jmsQueue)
                                                     .routeId("NormalizeMessageData");
            }
        };
    }

    @Bean
    public RouteBuilder httpToJMS() {
        return new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from("jetty:{{camel.jetty.orders.http.url}}").inOut("{{camel.jms.incoming.orders}}").tracing().routeId("HTTPtoJMS");
            }
        };
    }


}
