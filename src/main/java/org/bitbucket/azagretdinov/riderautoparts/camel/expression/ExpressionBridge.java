/*
 * Copyright 2016 Arthur Zagretdinov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.azagretdinov.riderautoparts.camel.expression;

import org.apache.camel.Exchange;
import org.apache.camel.Expression;

import java.util.function.Function;

/**
 *
 */
public class ExpressionBridge<V> implements Expression {

    public static <V> Expression function(Function<Exchange, V> function) {
        return new ExpressionBridge<>(function);
    }

    private final Function<Exchange, V> function;

    private ExpressionBridge(Function<Exchange, V> function) {
        this.function = function;
    }

    @Override
    public <T> T evaluate(Exchange exchange, Class<T> type) {
        return exchange.getContext().getTypeConverter().convertTo(type, function.apply(exchange));
    }
}