/*
 * Copyright 2016 Arthur Zagretdinov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.azagretdinov.riderautoparts.domain;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 */
public class OrderPosition implements Serializable {
    private String name;

    private int amount;

    public OrderPosition(String name, int amount) {
        this.name = name;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderPosition that = (OrderPosition) o;
        return getAmount() == that.getAmount() && Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getAmount());
    }


    @Override
    public String toString() {
        return "OrderPosition{" +
                "name='" + name + '\'' +
                ", amount=" + amount +
                '}';
    }
}
